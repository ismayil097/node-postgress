#!/bin/bash

# export PGPASSWORD = 'node_password'

database="monstersdb"

echo "Configuring database: $database"

dropdb monstersdb
createdb monstersdb

psql monstersdb < ./bin/sql/monsters.sql

echo "$database configured"
